from collections import OrderedDict
import json

from common import data_path


def main():
    with open(data_path, "r") as fp:
        old_data = json.load(fp)
    sorted_keys = [
        "versiontracker",
        "tags",
        "skip"
    ]
    new_data = OrderedDict()
    for key in sorted(old_data.keys()):
        new_data[key] = OrderedDict()
        for sorted_key in sorted_keys:
            if sorted_key in old_data[key]:
                new_data[key][sorted_key] = old_data[key][sorted_key]
        for subkey in [subkey for subkey in old_data[key]
                       if subkey not in old_data[key] and subkey != "wikidata"]:
            new_data[key][subkey] = old_data[key][subkey]
    with open(data_path, "w") as fp:
        json.dump(new_data, fp, indent=4, separators=(",", ": "))
        fp.write('\n')


if __name__ == "__main__":
    main()
