"""Report software that exists in the Chakra repositories that we do not
track."""

import re

from common import repository_packages, tracked_packages
from autofill import main as autofill

repositories = [
    "desktop",
    "gtk",
    "lib32",
]
autofill(repositories)
_repository_packages = repository_packages(repositories)
_tracked_packages = tracked_packages()
untracked_packages = set()
ignore_package_re = re.compile(
    "^(lib32-.*|.*-(bzr|cvs|hg|debug|git|log|svn))$")
for package in _repository_packages:
    if package not in _tracked_packages:
        if not ignore_package_re.match(package):
            untracked_packages.add(package)
for package in sorted(untracked_packages):
    print(package)
