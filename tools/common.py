import json
import os

from chakraversiontracker.__main__ import _fetch_repository_data


data_path = os.path.join(
    os.path.dirname(__file__), "..", "chakraversiontracker", "data.json")


def repository_packages(repositories):
    packages = set()
    for data in _fetch_repository_data(repositories).values():
        for package in data.keys():
            packages.add(package)
    return packages


def tracked_packages():
    return set(tracking_data().keys())


def tracking_data():
    with open(data_path, "r") as fp:
        return json.load(fp)
