"""Fills `data.json` with packages from `versiontracker` that have the same
name as packages in the Chakra repositories."""

from copy import deepcopy
import json
import re

from versiontracker import supported_software

from common import data_path, repository_packages, tracking_data
from lint import main as lint

_PACKAGE_AFFIX_RE = re.compile(
    "^(?:(?:haskell|perl|php|plasma5?-applets|python[23]?)-)?"
    "(.*?)"
    "(?:-(?:cli|common|data|docs?|gui|src))?$")
_PYTHON_PREFIX_RE = re.compile(r"^python[23]?-(.*)")


def main(repositories=("core", "desktop", "gtk", "lib32", "testing", "ccr")):
    with open(data_path) as f:
        data = json.load(f)
    _repository_packages = repository_packages(repositories)
    versiontracker_packages = supported_software()
    _tracking_data = tracking_data()
    for package in _repository_packages:
        if package not in _tracking_data:
            if package in versiontracker_packages:
                data[package] = {}
                continue
            if package.startswith('lib32-'):
                versiontracker_package = package[6:]
                if versiontracker_package in versiontracker_packages:
                    data[package] = {
                        "versiontracker": versiontracker_package
                    }
                    continue
                elif versiontracker_package in _tracking_data:
                    data[package] = deepcopy(
                        _tracking_data[versiontracker_package])
                    continue
            match = _PACKAGE_AFFIX_RE.search(package)
            if match:
                versiontracker_package = match.group(1)
                if versiontracker_package in versiontracker_packages:
                    data[package] = {
                        "versiontracker": versiontracker_package
                    }
                    continue
            match = _PYTHON_PREFIX_RE.search(package)
            if match:
                versiontracker_package = "python-" + match.group(1)
                if versiontracker_package in versiontracker_packages:
                    data[package] = {
                        "versiontracker": versiontracker_package
                    }
                    continue
                versiontracker_package = "py" + match.group(1)
                if versiontracker_package in versiontracker_packages:
                    data[package] = {
                        "versiontracker": versiontracker_package
                    }
                    continue
    with open(data_path, 'w') as f:
        json.dump(data, f)
    lint()


if __name__ == "__main__":
    main()
