"""Report software that we track using an invalid Version Tracker software
ID."""

from common import tracking_data
from versiontracker import supported_software

valid_software_ids = supported_software()
_tracking_data = tracking_data()

for package in sorted(_tracking_data.keys()):
    software_id = package
    if 'versiontracker' in _tracking_data[package]:
        software_id = _tracking_data[package]['versiontracker']
    if _tracking_data[package].get('skip', False) is True:
        continue
    if software_id not in valid_software_ids:
        if software_id == package:
            print(package)
        else:
            print(u"{} ({})".format(package, software_id))
