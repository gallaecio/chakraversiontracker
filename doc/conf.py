#!/usr/bin/env python3
# coding=utf-8

import os.path
import sys


_parent_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')
sys.path.insert(0, _parent_folder)
_version_path = os.path.join(_parent_folder, 'chakraversiontracker',
                             '_version.py')
with open(_version_path) as f:
    exec(f.read())

extensions = [
    'sphinxarg.ext',
]
templates_path = ['_templates']
source_suffix = ['.rst']
master_doc = 'index'
project = 'Chakra Vesion Tracker'
copyright = "2016, Adrián Chaves (Gallaecio)"
author = "Adrián Chaves (Gallaecio)"
version = __software_version__
release = __version__
language = None
exclude_patterns = ['_build']
pygments_style = 'sphinx'
todo_include_todos = False
html_theme = 'alabaster'
html_static_path = ['_static']
htmlhelp_basename = 'chakraversiontrackerdoc'
latex_elements = {}
latex_documents = [
    (master_doc, 'chakraversiontracker.tex',
     'Chakra Vesion Tracker Documentation',
     "Adrián Chaves (Gallaecio)", 'manual'),
]
man_pages = [
    (master_doc, 'chakraversiontracker', 'Chakra Vesion Tracker Documentation',
     [author], 1)
]
texinfo_documents = [
    (master_doc, 'chakraversiontracker', 'Chakra Vesion Tracker Documentation',
     author, 'chakraversiontracker', 'One line description of project.',
     'Miscellaneous'),
]
